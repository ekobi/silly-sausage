defmodule Mix.Tasks.Sausage do
  @moduledoc "Generate a silly-sausage name"
  @shortdoc "Generate a silly-sausage name"

  @words_url "https://www.wordfrequency.info/samples/lemmas_60k_words.txt"
  @banned_words_url "http://www.bannedwordlist.com/lists/swearWords.txt"

  use Mix.Task
  @impl Mix.Task

  NimbleCSV.define(MyParser, separator: "\t", escape: "\"")
  def run([]), do: run(["0"])
  def run([rank_string]) do
    {:ok, {_, _, banned_words_file}} = @banned_words_url |> :httpc.request
    banned_map =
      banned_words_file
      |> to_string
      |> String.split("\r\n")
      |> Enum.reduce(%{}, fn word, acc -> Map.put(acc, word, true) end)

    {:ok, {_, _, words_file}} = @words_url |> :httpc.request
    word_entries =
      words_file
      |> to_string
      |> String.split("\r\n")
      |> Enum.drop(8)
      |> Enum.filter(fn s -> String.length(s) > 0 end)

    rank_cutoff = String.to_integer(rank_string)
    %{"j" => adjective_tuples, "n" => noun_tuples} =
      word_entries
      |> MyParser.parse_enumerable
      |> Enum.map(fn [lem_rank, lemma, pos, _, _, _] -> {:binary.copy(lemma), :binary.copy(pos), String.to_integer(lem_rank)} end)
      |> Enum.filter(fn {_, _, lem_rank} -> (rank_cutoff == 0) || (lem_rank <= max(500, rank_cutoff)) end)
      |> Enum.filter(fn {_, pos, _} -> pos == "j" || pos == "n" end)
      |> Enum.dedup
      |> Enum.filter(fn {word, _, _} -> !Map.has_key?(banned_map, word) end)
      |> Enum.group_by(fn {_, pos, _} -> pos end)

    adjectives =
      adjective_tuples
      |> Enum.map(fn {adjective, "j", _} -> adjective end)
      #|> IO.inspect

    nouns =
      noun_tuples
      |> Enum.map(fn {noun, "n", _} -> noun end)
      #|> IO.inspect

    Mix.shell().info(
      inspect(%{nouns: length(nouns), adjectives: length(adjectives), banned: length(Map.keys(banned_map))}))

    [adjectives, nouns]
    |> Enum.map(fn word_list -> Enum.random(word_list) end)
    |> Enum.join("-")
    |> String.downcase
    |> Mix.shell().info

  end

  defmodule Eff do
    @eff_short_words_url "https://www.eff.org/files/2016/09/08/eff_short_wordlist_2_0.txt"
    use Mix.Task
    @impl Mix.Task

    def run(_) do
      {:ok, {_, _, short_words_file}} = @eff_short_words_url |> :httpc.request
      words =
        short_words_file
        |> to_string
        |> String.split("\n")
        |> Enum.filter(fn s -> String.length(s) > 0 end)
        |> Enum.map(fn <<_::binary-size(5), word::binary>> -> word end)

      [words, words]
      |> Enum.map(fn word_list -> Enum.random(word_list) end)
      |> Enum.join("-")
      |> String.downcase
      |> Mix.shell().info
    end


  end
end

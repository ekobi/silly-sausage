defmodule SillySausage do
  @moduledoc """
  Documentation for `SillySausage`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> SillySausage.hello()
      :world

  """
  def hello do
    :world
  end
end
